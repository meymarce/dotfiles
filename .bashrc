# aliases
if [ -f ~/.bash_aliases ]; then
	. ~/.bash_aliases
fi


# set default programs
export EDITOR=vim

## ssh agent gpg
#unset SSH_AGENT_PID
#if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
#	export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
#fi

# load other .bashrc things
for enabled in ~/.bashrc.d/*.enabled; do
	[ -e "$enabled" ] || break
	. $enabled
done
