alias dotfiles="git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME"

alias ls="ls -lah"

alias lxc-ls="lxc-ls -1f"
